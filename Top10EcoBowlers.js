
function bestEconomies2015(deliveries, matches) {
    // console.log(matches);
    var matches2015id = matches
      .filter((item) => item.season == 2015)
      .reduce((array, ind) => {
        array.push(ind.id);
        return array;
      }, []);
  
    var deliveries2015 = deliveries.filter((item) =>
      matches2015id.includes(item.match_id)
    );
  
    result = {};
   
    var count = 1;
    for (let item of deliveries2015) {
      result[item.bowler] =
        result[item.bowler] +
          (+item.wide_runs + +item.batsman_runs + +item.noball_runs) ||
        +item.wide_runs + +item.batsman_runs + +item.noball_runs;
    }
    
  
 
    
    var array = Object.entries(result);
   
   
    array.sort((a, b) => a[1] - b[1]);
    var array = array.slice(0, 10);
   
  
   return array;
  }
  module.exports=bestEconomies2015
  
  