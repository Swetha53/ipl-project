

function extraRun2016(deliveries, matches) {
    var matchesHeldInIn2016 = matches
      .filter((item) => item.season == 2016)
      .reduce((idsOf2016, ind) => {
        idsOf2016.push(ind.id);
        return idsOf2016;
      }, []);
  
    var deliveriesOf2016 = deliveries.filter((item) =>
      matchesHeldInIn2016.includes(item.match_id)
    );
  
    result = {};
    for (let item of deliveriesOf2016) {
      result[item.bowling_team] =
        result[item.bowling_team] + parseInt(item.extra_runs) ||
        +parseInt(item.extra_runs);
    }
return  result;
  }
  module.exports=extraRun2016