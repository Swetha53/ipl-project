
function  matcherPerYear(source)

{ {
    const result = {};
    for (let match of source) {
      const season = match.season;
      if (result[season]) {
        result[season] += 1;
      } else {
        result[season] = 1;
      }
    }
    return  result;
  }
  
 
 
}
module.exports=matcherPerYear;